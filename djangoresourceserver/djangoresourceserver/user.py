from django.contrib.auth import get_user_model


def jwt_get_username_from_payload_handler(payload):
    # Django requires a user that exists locally in order 
    # to associate future calls with
    # this is quick solution that should be dealt with 
    # more properly (ideally no local user needed at all?)
    sub = payload.get('sub')
    User = get_user_model()

    try:
        User.objects.get(username=sub)
    except:
        User(username=sub).save()

    return sub
