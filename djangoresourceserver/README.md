This app largely came from: https://auth0.com/docs/quickstart/backend/django/01-authorization


# Notes

- You can only have one additional audience at a time (ie environment), so when using the django endpoint over the nodejs endpoint, you need to switch what api audience is requested on the frontend app
