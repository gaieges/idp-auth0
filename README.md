# "Show how great auth0 is" repo

# Quickstart

```
docker-compose up --build
```

Then: http://localhost:3000

# Behind the scenes

This spins up a few (very simple) services:

- `react-authz`: 
    - React frontend that allows you to login to auth0 and make protected calls to backends below
    - NodeJS server app that verifies jwt/scope against openid JWKS at auth0
- `djangoresourceserver`
    - Django app  that verifies jwt/scope against JWKS at auth0



# Questions, answered

### We need to be able to customize login pages, language, and the visuals

We can pretty easily do this by using auth0's [lock tool](https://auth0.com/docs/libraries/lock/v11) which provides us with the smooth default auth0 login screen deployed via the content within the `pages` folder of this repo.  A bulk of the styling aside (but less of the JS behavior) is customizable.

Alternatively, we can make our own custom pages and just call the [functions provided by small auth0 lib](https://github.com/auth0/auth0.js) to perform the auth0 specific steps (ie authenticate username password, present dialogs / challenges)





# Reference 

## Initial setup in auth0

- Created an auth0 tenant
- Created a client within it
- Got a repo for examples (`react-authz` dir)
    - Plugged client creds into `react-authz` app
    - Customized requested scope (hudya.protected and messages:write)
- Created API on auth0 site (http://localhost:3000)
    - Customized requested scope (hudya.protected and messages:write)
- Created group, role, permission that allowed those users to get those scopes


## What's needed for migration

- setup auth to create and manage users in auth0
    - on otp verify, submit “verify phone”, and store phone # in metadata field
    - on login, perform client credentials password auth flow
    - getusertoken to impersonate user
- create import script to put to auth0 on migration 
- set consumer creation in kong to use public keys now
    - eventually push the ms’s to do the validation (and necessary for getusertoken)
- set kevin to use auth0 lib, request educator role, get scope of getusertoken
