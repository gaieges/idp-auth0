# In order to be a viable candidate

- need to be able to let educators for example to login and change what user is in what group
- is it possible to go with the super simple login (ie provide email or phone), and collect other info later?  
    - can we progressively do that?
- can we integrate more seamlessly the login to signup app? 

